<?php

/**
 * @file
 * An API module providing by AUL(Access User Lists).
 *
 * @todo: change description.
 * This module handles AULs on behalf of other modules. The two main reasons
 * to do this are so that modules using AULs can share them with each
 * other without having to actually know much about them, and so that
 * AULs can easily co-exist with the existing node_access system.
 * Most of the functions in this file are for AUL CRUD operations.
 * 
 * @todo: implement bulk grants update.
 * @todo: add realm explenations.
 * @todo: access to node revisions.
 */

module_load_include('inc', 'aul', 'includes/aul');

/**
 * Create a new AUL.
 *
 * The client module will have to keep track of the AUL. For that it can assign
 * $source to this AUL.
 * 
 * @param int $key_id
 *   Id of key entity to grant permissions for. E.g. user id, role id etc.
 * @param string $key_name
 *   Helps to formulate realm name. Could be 'uid', 'rid' etc.
 * @param int $nid
 *   The {node}.nid to which this {gid} entry applies.
 * @param string $op
 *   Operation name
 * @param string $source
 *   Source of AUL data. Module name or field name. Works as namespace.
 * @param string $value
 *   Number of access assignment by entity references.
 * 
 * @return int
 *   New AUL grant id.
 */
function aul_create_aul($key_id, $key_name, $nid, $op, $source, $value = 0) {
  
  $fields = array(
    'op' => $op,
    'key_name' => $key_name,
    'key_id' => (int) $key_id,
    'nid' => (int) $nid,
    'value' => (int) $value,
    'source' => $source,
  );
  
  $gid = db_insert('aul')
   ->fields($fields)
   ->execute();
  
  return $gid;
}

/**
 * Gets AUL record using certain conditions.
 *
 * @param array $conditions
 *   - 'op' (string): Operation name
 *   - 'key_name' (string): Helps to formulate realm name. Could be 'uid', 'rid'
 *     etc.
 *   - 'key_id' (int): Id of key entity to grant permissions for. E.g. user id,
 *     role id etc.
 *   - 'nid' (int): The {node}.nid to which this {gid} entry applies.
 *   - 'value' (string): Number of access assignment by entity references.
 *   - 'source' (string): (optional) Source of AUL data. Module name or field
 *     name. Works as namespace for AUL grants.
 *
 * @return object
 *   AUL record.
 */
function aul_get_aul(array $conditions) {

  // Exit if none conditions is set.
  if (empty($conditions)) {
    return;
  }

  $conditions_empty = TRUE;
  $items = array('gid', 'op', 'key_name', 'key_id', 'nid', 'value', 'source');
  foreach (array_keys($conditions) as $condition_kay) {
    if (in_array($condition_kay, $items)) {
      $conditions_empty = FALSE;
      break;
    }
  }
  if($conditions_empty) {
    return;
  }

  $query = db_select('aul', 'a');
  $query->fields('a', array('gid', 'op', 'key_name', 'key_id', 'nid', 'value', 'source'));

  // Add query conditions.
  foreach ($items as $item) {
    if (isset($conditions[$item])) {
      $query->condition($item, $conditions[$item]);
    }
  }

  return $query->execute()->fetchAll();
}

/**
 * Get the id of an AUL by name (+ optionally number).
 *
 * @param array $conditions
 *   - 'op' (string): Operation name
 *   - 'key_name' (string): Helps to formulate realm name. Could be 'uid', 'rid'
 *     etc.
 *   - 'key_id' (int): Id of key entity to grant permissions for. E.g. user id, 
 *     role id etc.
 *   - 'nid' (int): The {node}.nid to which this {gid} entry applies.
 *   - 'value' (string): Number of access assignment by entity references.
 *   - 'source' (string): (optional) Source of AUL data. Module name or field 
 *     name. Works as namespace for AUL grants.
 * 
 * @return int
 *   AUL id if fount
 */
function aul_get_id(array $conditions) {
  // Exit if conditions are not set.
  if (empty($conditions)) {
    return;
  }
  
  $conditions_empty = TRUE;
  $items = array('op', 'key_name', 'key_id', 'nid', 'value', 'source');
  foreach (array_keys($conditions) as $condition_kay) {
    if (in_array($condition_kay, $items)) {
      $conditions_empty = FALSE;
      break;
    }
  }
  if($conditions_empty) {
    return;
  }

  $query = db_select('aul', 'a');
  $query->fields('a', array('gid'));
  
  // Add query conditions.
  foreach ($items as $item) {
    if (isset($conditions[$item])) {
      $query->condition($item, $conditions[$item]);
    }
  }

  return $query->execute()->fetchField();
}

/**
 * Get the gids of an AUL by name (+ optionally number).
 *
 * @param array $conditions
 *   - 'op' (string): Operation name
 *   - 'key_name' (string): Helps to formulate realm name. Could be 'uid', 'rid'
 *     etc.
 *   - 'key_id' (int): Id of key entity to grant permissions for. E.g. user id,
 *     role id etc.
 *   - 'nid' (int): The {node}.nid to which this {gid} entry applies.
 *   - 'value' (string): Number of access assignment by entity references.
 *   - 'source' (string): (optional) Source of AUL data. Module name or field
 *     name. Works as namespace for AUL grants.
 *
 * @return int
 *   AUL id if fount
 */
function aul_get_gids(array $conditions) {
  // Exit if conditions are not set.
  if (empty($conditions)) {
    return;
  }

  $conditions_empty = TRUE;
  $items = array('op', 'key_name', 'key_id', 'nid', 'value', 'source');
  foreach (array_keys($conditions) as $condition_kay) {
    if (in_array($condition_kay, $items)) {
      $conditions_empty = FALSE;
      break;
    }
  }
  if($conditions_empty) {
    return;
  }

  $query = db_select('aul', 'a');
  $query->fields('a', array('gid'));

  // Add query conditions.
  foreach ($items as $item) {
    if (isset($conditions[$item])) {
      $query->condition($item, $conditions[$item]);
    }
  }

  return $query->execute()->fetchCol();
}

/**
 * Get AUL id by user name, module name and operation. If AUL doesn't exist
 * creates new one.
 * 
 * @param int $key_id
 *   Id of key entity to grant permissions for. E.g. user id, role id etc.
 * @param string $key_name
 *   Helps to formulate realm name. Could be 'uid', 'rid' etc.
 * @param $nid
 *   Node id.
 * @param string $op
 *   Operation name
 * @param string $source
 *   Source of AUL data. Module name or field name. Works as 
 *   namespace for AUL grants.
 * 
 * @return int
 *   AUL grant id.
 */
function aul_get_id_or_create($key_id, $key_name, $nid, $op, $source) {
 
  $conditions = array(
    'key_id' => $key_id,
    'key_name' => $key_name,
    'nid' => $nid,
    'op' => $op,
    'source' => $source,
  );

  if (!$gid = aul_get_id($conditions)) {
    $gid = aul_create_aul($key_id, $key_name, $nid, $op, $source);
  }

  return $gid;
}

/**
 * Get all AUL grant ids of node.
 *
 * @param int $nid
 *   node id
 * @param string $source
 *   (optional) Source of AUL data. Module name or field name. Works as
 *   namespace for AUL grants.
 *
 * @return array
 *   array of AUL grant ids
 */
function aul_get_all_aul_of_node($nid, $source = NULL) {
  $gids = aul_get_gids(array(
    'nid' => $nid,
    'source' => $source,
  ));

  return $gids;
}

/**
 * Get all AUL grant ids of user.
 *
 * @param int $uid
 *   user id
 * @param string $source
 *   (optional) Source of AUL data. Module name or field name. Works as
 *   namespace for AUL grants.
 *
 * @return array
 *   array of AUL grant ids
 */
function aul_get_all_aul_of_user($uid, $source = NULL) {
  $gids = aul_get_gids(array(
    'key_id' => $uid,
    'key_name' => 'uid',
    'source' => $source,
  ));

  return $gids;
}

/**
 * Remove AUL with extended conditions for all the 'aul' table fields.
 * 
 * @param array $conditions
 *   - 'op' (string): Operation name
 *   - 'key_name' (string): Helps to formulate realm name. Could be 'uid', 'rid'
 *     etc.
 *   - 'key_id' (int): Id of key entity to grant permissions for. E.g. user id, 
 *     role id etc.
 *   - 'nid' (int): The {node}.nid to which this {gid} entry applies.
 *   - 'value' (string): Number of access assignment by entity references.
 *   - 'source' (string): (optional) Source of AUL data. Module name or field 
 *     name. Works as namespace for AUL grants.
 */
function aul_delete_aul($conditions) {
  
  // Exit if none conditions is set.
  if (empty($conditions)) {
    return;
  }
  
  $conditions_empty = TRUE;
  $items = array('op', 'key_name', 'key_id', 'nid', 'value', 'source');
  foreach (array_keys($conditions) as $condition_kay) {
    if (in_array($condition_kay, $items)) {
      $conditions_empty = FALSE;
      break;
    }
  }
  if($conditions_empty) {
    return;
  }

  $query = db_delete('aul');
  
  // Add query conditions.
  foreach ($items as $item) {
    if (isset($conditions[$item])) {
      $query->condition($item, $conditions[$item]);
    }
  }
  
  $query->execute();
    
  // @todo: hook_aul_delete_aul().
  // Let other modules react on removing AUL grants.
  module_invoke_all('aul_delete_aul', $conditions);
}

/**
 * Delete an existing AUL by id.
 * 
 * @param int $gid
 *   AUL grant id.
 */
function aul_delete_aul_by_id($gid) {
  
  $query = db_delete('aul');
  $query->condition('gid', $gid);
  $query->execute();
}

/**
 * Delete existing AULs multiple.
 * 
 * @param array $gids
 *   array of AUL grant ids.
 */
function aul_delete_aul_by_id_multiple(array $gids) {

  $query = db_delete('aul');
  $query->condition('gid', $gids, 'IN');
  $query->execute();
}

/**
 * Remove all AUL of a node.
 * 
 * @param int $nid
 *   Node id.
 * @param string $source
 *   (optional) Source of AUL data. Module name or field name. Works as 
 *   namespace for AUL grants.
 */
function aul_node_delete_auls($nid, $source = NULL) {
  // Allow other modules to react before node AUL deletion.
  module_invoke_all('aul_node_pre_delete', $nid);

  $query = db_delete('aul');
  $query->condition('nid', $nid);
  if($source) {
    $query->condition('source', $source = NULL);
  }
  $query->execute();
}

/**
 * Remove all AUL of a user.
 * 
 * @param int $uid
 *   User id.
 * @param string $source
 *   (optional) Source of AUL data. Module name or field name. Works as 
 *   namespace for AUL grants.
 */
function aul_user_delete_auls($uid, $source = NULL) {
  // Allow other modules to react before user AUL deletion.
  module_invoke_all('aul_user_pre_delete', $uid);

  $query = db_delete('aul');
  $query->condition('key_name', 'uid');
  $query->condition('key_id', $uid);
  if($source) {
    $query->condition('source', $source = NULL);
  }
  $query->execute();
}

/**
 * Set value to AUL grant.
 * 
 * @param int $gid
 *   AUL grant id
 * @param int $value
 *   AUL grant value
 */
function aul_value_set($gid, $value) {
  
  db_update('aul')
    ->expression('value', $value)
    ->condition('gid', $gid)
    ->execute();
}

/**
 * Implements hook_node_access_records().
 */
function aul_node_access_records($node) {
  
  if (empty($node->nid)) {
    return;
  }

  $results = db_select('aul', 'a')
   ->fields('a')
   ->condition('nid', $node->nid)
   ->condition('value', 0, '>')
   ->execute()
   ->fetchAll();
  
  $grants = array();
  foreach ($results as $result) {
    // @todo: Grants priority & source. What if we have equal realms for 
    // different sources? Priority or merging could help.
    $grant = array(
      'realm' => "aul_{$result->op}_{$result->key_name}_{$result->key_id}",
      'gid' => $result->gid,
      'grant_view' => 0,
      'grant_update' => 0,
      'grant_delete' => 0,
      'priority' => 0,
      '#module' => 'AUL',
    );
    $grant["grant_{$result->op}"] = $result->value;

    $grants[] = $grant;
  }
  
  return $grants;
}

/**
 * Implements hook_node_grants().
 */
function aul_node_grants($account, $op) {

  $gids = db_select('aul', 'a')
   ->fields('a', array('gid'))
   ->condition('a.key_name', 'uid')
   ->condition('a.key_id', $account->uid)
   ->condition('a.op', $op)
   ->condition('a.value', 0, '>')
   ->execute()
   ->fetchCol();
  
  if (!empty($gids)) {
    
    $realm = "aul_{$op}_uid_{$account->uid}";
    
    $keys = array(
      $realm => $gids,
    );
    
    return $keys;
  }
}

/**
 * Implements hook_node_delete().
 */
function aul_node_delete($node) {
  aul_node_delete_auls($node->nid);
}

/**
 * Implements hook_user_cancel().
 */
function aul_user_cancel($edit, $account, $method) {
  
  aul_user_delete_auls($account->uid);
}

/**
 * Implements hook_user_delete().
 */
function aul_user_delete($account) {
  
  aul_user_delete_auls($account->uid);
}
