<?php

/**
 * @file
 * A UI module providing basic UI functionality to manage AUL access over the UI
 *
 */

/**
 * Implements hook_menu().
 */
function aul_ui_menu() {
  $items['node/%node/aul'] = array(
    'title' => 'AUL Access',
    'page callback' => 'aul_ui_node_aul',
    'page arguments' => array(1),
    'access callback' => 'aul_ui_node_aul_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  $items['node/%node/aul/%user/%/delete'] = array(
    'page callback' => 'aul_ui_node_aul_delete',
    'page arguments' => array(1, 3, 4),
    'access callback' => 'aul_ui_node_aul_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_permissions().
 */
function aul_ui_permission() {
  // For every node type we introduce a new permission to manage AUL access.
  $types = node_type_get_types();
  $items = array();
  foreach ($types as $type) {;
    $perm_name = _aul_ui_get_permission($type->type);
    $items[$perm_name] = array(
      'title' => t('Manage !type_name aul access', array('!type_name' => htmlspecialchars($type->name))),
    );
  }
  return $items;
}
/**
 * Access callback. Returns the permission name for a given node.
 *
 * @param string $node_type
 *   Node type name.
 *
 * @return string
 *   Permission name.
 */
function _aul_ui_get_permission($node_type) {
  return 'manage '.$node_type.' aul access';
}

/**
 * Access callback for url to manage AULs of a node
 */
function aul_ui_node_aul_access($node) {
  if (empty($node->nid)) {
    return FALSE;
  }

  return user_access(_aul_ui_get_permission($node->type)) && node_access('view', $node);
}
/**
 * Callback to manage AUL of a node.
 *
 * Adds form to create new access grants for a user for this node.
 */
function aul_ui_node_aul($node) {
  $title = t('Manage access for !node', array('!node' => $node->title));
  drupal_set_title($title);
  $form = drupal_get_form('_aul_ui_add_grants_form', $node);

  return $form;
}

/**
 * Deletes grants of a user for a given node.
 */
function aul_ui_node_aul_delete($node, $user, $grant_type) {
  if (!isset($_GET['token']) || !drupal_valid_token($_GET['token'], "delete/aul/$grant_type/$user->uid/$node->nid")) {
    return MENU_ACCESS_DENIED;
  }

  drupal_set_message(t('Grant !grant of !node was deleted for !user',
    array(
      '!grant' => $grant_type,
      '!node' => l($node->title, 'node/'.$node->nid),
      '!user' => l($user->name, 'user/'.$user->uid),
    )
  ));

  // Delete the grants.
  aul_remove_aul($user->uid, $node->nid, array($grant_type => TRUE), AUL_DEFAULT_SOURCE);
  node_access_acquire_grants($node);

  drupal_goto('node/'.$node->nid.'/aul');
}

/**
 * Returns the grants list for a node.
 */
function _aul_ui_get_grants_list($grants, $nid) {
  $rows = array();

  // Prepare table header.
  $header = array(t('user'), t('grant'), t('operation'));

  $grant_types = array('view', 'update', 'delete');
  foreach ($grants as $uid=>$grant_arr) {
    if(!$a_user = user_load($uid)) {
      continue;
    }
    foreach ($grant_types as $grant_type) {
      if (!empty($grant_arr[$grant_type])) {
        $token =  drupal_get_token("delete/aul/$grant_type/$uid/$nid");
        $delete_link = l(t('delete'), "node/$nid/aul/$uid/$grant_type/delete", array('query' => array('token' => $token)));
        $a_user = user_load($uid);
        $user_link = l($a_user->name, 'user/' . $a_user->uid);
        $rows[] = array($user_link, $grant_type, $delete_link);
      }
    }
  }

  // No results data.
  if (!$rows) {
    $rows[]['data'][] = array(
      'data' => t('No AUL grants assigned yet'),
      'colspan' => 3,
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Returns all grants of all users for the give node
 */
function _aul_ui_get_grants($nid) {
  // Get all users with AUL grants of this node.
  $users = aul_get_uids_with_grants($nid, AUL_DEFAULT_SOURCE);

  $result = array();
  $grant_types = array('view', 'update', 'delete');
  foreach ($users as $uid => $grants) {
    // Get grants of user by this AUL.
    foreach ($grant_types as $grant_type) {
      if (!empty($grants[$grant_type])) {
        $result[$uid][$grant_type] = $grants[$grant_type];
      }
    }
  }

  return $result;
}

/**
 * Returns the form to add new grants.
 */
function _aul_ui_add_grants_form($form, &$form_state, $node) {
  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#size' => 30,
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#required' => true,
    '#description' => t('The user to add gratns to'),
  );

  $form['grants'] = array(
    '#type' => 'checkboxes',
    '#required' => true,
    '#title' => t('Grants'),
    '#options' => array('view' => t('view node'), 'update' => t('update node'), 'delete' => t('delete node')),
    '#description' => t('The grants the user will be given.'),
  );

  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Get current AUL data.
  $current_grants = _aul_ui_get_grants($node->nid);
  // Create table to show current grants and edit them.
  $table = _aul_ui_get_grants_list($current_grants, $node->nid);

  $form['data_table'] = array(
    '#type' => 'markup',
    '#markup' => $table,
  );

  return $form;
}

/**
 * Validate callback for AUL grants UI form.
 */
function _aul_ui_add_grants_form_validate($form, $form_state) {
  if (!user_load_by_name($form_state['values']['user'])) {
    form_set_error('user', t('Please enter a valid user name.'));
  }
}

/**
 * Submit handler of add aul grants form.
 */
function _aul_ui_add_grants_form_submit($form, $form_state) {
  $values = $form_state['values'];

  $username = $values['user'];
  $grants = $values['grants'];
  $nid = $values['nid'];

  // Now add these grants.
  $user = user_load_by_name($username);
  aul_add_aul($user->uid, $nid, $grants, AUL_DEFAULT_SOURCE);
  $node = node_load($nid);
  node_access_acquire_grants($node);

  $message = t('Access to %grants "%title" has been successfully added for %user.', array(
    '%user' => $user->name,
    '%title' => $node->title,
    '%grants' => implode(', ', array_filter($grants)),
  ));

  drupal_set_message($message);
}
